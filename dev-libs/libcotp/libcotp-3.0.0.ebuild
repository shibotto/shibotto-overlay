# Copyright 2020-2024 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

inherit cmake

DESCRIPTION="C library that generates TOTP and HOTP"
HOMEPAGE="https://github.com/paolostivanin/libcotp"
SRC_URI="https://github.com/paolostivanin/${PN}/archive/v${PV}.tar.gz -> ${P}.tar.gz"

LICENSE="Apache-2.0"
SLOT="0/3"
KEYWORDS="amd64 ~x86"
IUSE="+gcrypt test"
RESTRICT="!test? ( test )"

RDEPEND="
	!gcrypt? ( >=dev-libs/openssl-3.0.0:= )
	gcrypt? ( >=dev-libs/libgcrypt-1.8.0:= )
"
DEPEND="
	${RDEPEND}
	test? ( dev-libs/criterion )
"
BDEPEND="virtual/pkgconfig"

src_configure() {
	local mycmakeargs=(
		-DBUILD_TESTS=$(usex test ON OFF)
		-DHMAC_WRAPPER=$(usex gcrypt gcrypt openssl)
	)

	cmake_src_configure
}
