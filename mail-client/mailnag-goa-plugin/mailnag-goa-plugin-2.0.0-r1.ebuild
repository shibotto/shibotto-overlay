# Copyright 2019-2024 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

DISTUTILS_SINGLE_IMPL=1
DISTUTILS_USE_PEP517=setuptools
PYTHON_COMPAT=( python3_{9,10,11,12} )
inherit distutils-r1

DESCRIPTION="Mailnag GNOME Online Accounts plugin "
HOMEPAGE="https://github.com/pulb/mailnag-goa-plugin"
SRC_URI="https://github.com/pulb/${PN}/archive/v${PV}.tar.gz -> ${P}.tar.gz"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="amd64 ~x86"
IUSE=""

RDEPEND=">=mail-client/mailnag-2[${PYTHON_SINGLE_USEDEP}]
	net-libs/gnome-online-accounts[introspection]"
