# Copyright 2019-2024 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

DISTUTILS_USE_PEP517=setuptools
DISTUTILS_SINGLE_IMPL=1
PYTHON_COMPAT=( python3_{9,10,11,12} )
inherit distutils-r1 optfeature xdg

DESCRIPTION="An extensible mail notification daemon"
HOMEPAGE="https://github.com/pulb/mailnag"
SRC_URI="https://github.com/pulb/${PN}/archive/v${PV}.tar.gz -> ${P}.tar.gz"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="amd64 ~x86"

RDEPEND="
	app-crypt/libsecret[introspection]
	dev-libs/gobject-introspection
	media-libs/gst-plugins-base:1.0[introspection]
	x11-libs/gdk-pixbuf:2[introspection]
	x11-libs/gtk+:3[introspection]
	>=x11-libs/libnotify-0.7.6[introspection]
	$(python_gen_cond_dep '
		dev-python/dbus-python[${PYTHON_USEDEP}]
		dev-python/pygobject:3[${PYTHON_USEDEP}]
		dev-python/pyxdg[${PYTHON_USEDEP}]
	')
"
BDEPEND="sys-devel/gettext"

PATCHES=( "${FILESDIR}/${PN}-2.2.0-python3_12.patch" )

pkg_postinst() {
	xdg_pkg_postinst

	optfeature "GNOME Online Accounts integration" mail-client/mailnag-goa-plugin
}
