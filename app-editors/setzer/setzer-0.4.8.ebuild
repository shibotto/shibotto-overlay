# Copyright 2021-2023 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

PYTHON_COMPAT=( python3_{9,10,11} )
inherit meson python-single-r1 xdg

MY_PN="Setzer"

DESCRIPTION="Simple yet full-featured LaTeX editor"
HOMEPAGE="https://www.cvfosammmm.org/setzer/"
SRC_URI="https://github.com/cvfosammmm/${MY_PN}/archive/v${PV}.tar.gz -> ${P}.tar.gz"

LICENSE="GPL-3+"
SLOT="0"
KEYWORDS="amd64 ~x86"
IUSE="test"
RESTRICT="!test? ( test )"
REQUIRED_USE="${PYTHON_REQUIRED_USE}"

RDEPEND="
	${PYTHON_DEPS}
	app-text/gspell[introspection]
	app-text/poppler[cairo,introspection]
	|| (
		net-libs/webkit-gtk:4.1[introspection]
		net-libs/webkit-gtk:4[introspection]
	)
	virtual/latex-base
	x11-libs/gtksourceview:4[introspection]
	x11-libs/gtk+:3[introspection]
	x11-libs/pango[introspection]
	$(python_gen_cond_dep '
		dev-python/pexpect[${PYTHON_USEDEP}]
		dev-python/pygobject[${PYTHON_USEDEP}]
	')
"
BDEPEND="
	${PYTHON_DEPS}
	sys-devel/gettext
	test? ( dev-libs/appstream )
"

PATCHES=(
	"${FILESDIR}/${PN}-0.3.9-fix-python.patch"
	"${FILESDIR}/${P}-use-webkit-4.1-or-4.0.patch"
)

S="${WORKDIR}/${MY_PN}-${PV}"

src_install() {
	meson_src_install
	python_fix_shebang "${D}"
	python_optimize
}
