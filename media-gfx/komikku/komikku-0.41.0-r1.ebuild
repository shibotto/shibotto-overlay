# Copyright 2019-2024 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

PYTHON_COMPAT=( python3_{9,10,11,12} )
PYTHON_REQ_USE="sqlite"
inherit gnome2-utils meson python-single-r1 xdg

MY_P="Komikku-v${PV}"

DESCRIPTION="An online/offline manga reader for GNOME"
HOMEPAGE="https://gitlab.com/valos/Komikku"
SRC_URI="https://gitlab.com/valos/Komikku/-/archive/v${PV}/${MY_P}.tar.bz2 -> ${P}.tar.bz2"

LICENSE="GPL-3+"
SLOT="0"
KEYWORDS="amd64 ~x86"
REQUIRED_USE="${PYTHON_REQUIRED_USE}"

DEPEND="
	>=gui-libs/libhandy-1.5.0:1[introspection]
	x11-libs/gtk+:3[introspection]
"
RDEPEND="
	${PYTHON_DEPS}
	${DEPEND}
	|| (
		net-libs/webkit-gtk:4.1[introspection]
		net-libs/webkit-gtk:4[introspection]
	)
	x11-libs/libnotify[introspection]
	x11-libs/gdk-pixbuf[jpeg]
	$(python_gen_cond_dep '
		dev-python/beautifulsoup4[${PYTHON_USEDEP}]
		dev-python/brotlicffi[${PYTHON_USEDEP}]
		dev-python/cloudscraper[${PYTHON_USEDEP}]
		>=dev-python/dateparser-1.1.4[${PYTHON_USEDEP}]
		dev-python/emoji[${PYTHON_USEDEP}]
		>=dev-python/keyring-21.6.0[${PYTHON_USEDEP}]
		dev-python/lxml[${PYTHON_USEDEP}]
		dev-python/natsort[${PYTHON_USEDEP}]
		dev-python/pillow[jpeg,${PYTHON_USEDEP}]
		dev-python/pure-protobuf[${PYTHON_USEDEP}]
		dev-python/pygobject[cairo,${PYTHON_USEDEP}]
		dev-python/python-magic[${PYTHON_USEDEP}]
		dev-python/requests[${PYTHON_USEDEP}]
		dev-python/unidecode[${PYTHON_USEDEP}]
	')
"
BDEPEND="
	${PYTHON_DEPS}
	dev-libs/appstream-glib
	sys-devel/gettext
	virtual/pkgconfig
"

# Last updated: 2023-01-21
PATCHES=(
	# multi
	"${FILESDIR}/${P}-update-madara.patch"
	"${FILESDIR}/${P}-update-pizzareader.patch"
	# servers
	"${FILESDIR}/${P}-update-atikrost.patch"
	"${FILESDIR}/${P}-update-coloredcouncil.patch"
	"${FILESDIR}/${P}-update-dbmultiverse.patch"
	"${FILESDIR}/${P}-update-dynasty.patch"
	#"${FILESDIR}/${P}-update-japscan.patch"
	"${FILESDIR}/${P}-disable-kireicake.patch"
	"${FILESDIR}/${P}-update-leomanga.patch"
	"${FILESDIR}/${P}-update-leviatanscans.patch"
	"${FILESDIR}/${P}-update-mangadex.patch"
	"${FILESDIR}/${P}-update-mangafreak.patch"
	"${FILESDIR}/${P}-update-mangakawaii.patch"
	"${FILESDIR}/${P}-update-manganelo.patch"
	"${FILESDIR}/${P}-update-mangaowl.patch"
	"${FILESDIR}/${P}-update-mangascantrad.patch"
	"${FILESDIR}/${P}-update-mangasorigines.patch"
	"${FILESDIR}/${P}-update-ninemanga.patch"
	#"${FILESDIR}/${P}-update-readcomiconline.patch"
	"${FILESDIR}/${P}-update-readmanga.patch"
	"${FILESDIR}/${P}-update-reaperscans.patch"
	"${FILESDIR}/${P}-update-remanga.patch"
	"${FILESDIR}/${P}-update-scanfr.patch"
	"${FILESDIR}/${P}-update-tuttoanimemanga.patch"
	"${FILESDIR}/${P}-update-xoxocomics.patch"
	"${FILESDIR}/${P}-disable-zeroscans.patch"
)

S="${WORKDIR}/${MY_P}"

src_prepare() {
	# Manually set Python path
	sed -i "s:py_installation.path():'$PYTHON':" bin/meson.build || die

	# Fix Meson tests with network-sandbox
	sed -i "s/'validate-relax'/'--nonet', 'validate-relax'/" data/meson.build || die

	default
}

src_install() {
	meson_src_install
	python_optimize
}

pkg_postrm() {
	xdg_pkg_postrm
	gnome2_schemas_update
}

pkg_postinst() {
	xdg_pkg_postinst
	gnome2_schemas_update
}
