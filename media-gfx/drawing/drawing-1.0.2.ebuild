# Copyright 2021-2024 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

PYTHON_COMPAT=( python3_{9,10,11,12} )
inherit gnome2-utils meson python-single-r1 xdg

DESCRIPTION="A drawing application for the GNOME desktop"
HOMEPAGE="https://maoschanz.github.io/drawing/"
SRC_URI="https://github.com/maoschanz/${PN}/archive/${PV}.tar.gz -> ${P}.tar.gz"

LICENSE="GPL-3"
SLOT="0"
KEYWORDS="amd64 ~x86"
REQUIRED_USE="${PYTHON_REQUIRED_USE}"

DEPEND="dev-libs/glib"
RDEPEND="
	${PYTHON_DEPS}
	${DEPEND}
	x11-libs/gdk-pixbuf:2[jpeg]
	x11-libs/gtk+:3[introspection]
	$(python_gen_cond_dep '
		dev-python/pygobject[cairo,${PYTHON_USEDEP}]
	')
"
BDEPEND="
	${PYTHON_DEPS}
	dev-libs/appstream-glib
	dev-util/itstool
	sys-devel/gettext
	virtual/pkgconfig
"

PATCHES=( "${FILESDIR}/${PN}-0.6.5-fix-python.patch" )

src_prepare() {
	# Fix Meson tests with network-sandbox
	sed -i "s/'validate'/'--nonet', 'validate'/" data/meson.build || die

	default
}

src_install() {
	meson_src_install
	python_fix_shebang "${D}"/usr/bin/drawing
	python_optimize
}

pkg_postrm() {
	gnome2_schemas_update
	xdg_pkg_postrm
}

pkg_postinst() {
	gnome2_schemas_update
	xdg_pkg_postinst
}
