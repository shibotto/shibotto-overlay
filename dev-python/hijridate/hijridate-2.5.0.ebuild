# Copyright 2019-2024 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

DISTUTILS_USE_PEP517=hatchling
PYTHON_COMPAT=( python3_{9,10,11,12} )
inherit distutils-r1

DESCRIPTION="Python package to convert between Hijri and Gregorian dates"
HOMEPAGE="https://github.com/dralshehri/hijridate"
SRC_URI="https://github.com/dralshehri/${PN}/archive/v${PV}.tar.gz -> ${P}.gh.tar.gz"

LICENSE="MIT"
SLOT="0"
KEYWORDS="amd64 ~x86"

BDEPEND="dev-python/hatch-fancy-pypi-readme[${PYTHON_USEDEP}]"

distutils_enable_tests pytest
