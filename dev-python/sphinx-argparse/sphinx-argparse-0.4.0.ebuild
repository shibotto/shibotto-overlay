# Copyright 2021-2023 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

DISTUTILS_USE_PEP517=poetry
PYTHON_COMPAT=( python3_{9,10,11} )

inherit distutils-r1

DESCRIPTION="Sphinx extension that automatically document argparse commands and options"
HOMEPAGE="https://github.com/ashb/sphinx-argparse"
SRC_URI="https://github.com/ashb/${PN}/archive/refs/tags/v${PV}.tar.gz -> ${P}.gh.tar.gz"
#SRC_URI="mirror://pypi/${PN:0:1}/${PN}/${P}.tar.gz"

LICENSE="MIT"
SLOT="0"
KEYWORDS="amd64 ~x86"

RDEPEND="
	dev-python/commonmark[${PYTHON_USEDEP}]
	dev-python/sphinx[${PYTHON_USEDEP}]
"
BDEPEND="test? ( dev-python/six[${PYTHON_USEDEP}] )"

distutils_enable_sphinx docs \
	'dev-python/sphinx-argparse' \
	'dev-python/sphinx-rtd-theme'
distutils_enable_tests pytest

src_prepare () {
	# Fix building docs, although the referenced scripts are not installed
	sed -i "s;filename: ../test;filename: test;g" docs/{markdown,sample}.rst || die

	distutils-r1_src_prepare
}
