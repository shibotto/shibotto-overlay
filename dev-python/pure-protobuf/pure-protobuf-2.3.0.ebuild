# Copyright 2020-2023 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

PYTHON_COMPAT=( python3_{9,10,11} )
DISTUTILS_USE_PEP517=hatchling
inherit distutils-r1

MY_PN="protobuf"

DESCRIPTION="Python implementation of Protocol Buffers data types"
HOMEPAGE="https://github.com/eigenein/protobuf"
SRC_URI="https://github.com/eigenein/${MY_PN}/archive/${PV}.tar.gz -> ${P}.gh.tar.gz"

LICENSE="MIT"
SLOT="0"
KEYWORDS="amd64 ~x86"

BDEPEND="dev-python/hatch-vcs[${PYTHON_USEDEP}]"

S="${WORKDIR}/${MY_PN}-${PV}"

distutils_enable_tests pytest

export SETUPTOOLS_SCM_PRETEND_VERSION=${PV}

python_test() {
	local EPYTEST_DESELECT=(
		# Require pytest-benchmark which I'm not going to do
		'tests/test_serializers.py::test_unsigned_varint_serializer_dumps'
		'tests/test_serializers.py::test_unsigned_varint_serializer_loads'
		'tests/test_serializers.py::test_signed_varint_serializer_dumps'
		'tests/test_serializers.py::test_signed_varint_serializer_loads'
		'tests/test_serializers.py::test_twos_compliment_64_serializer_dumps'
		'tests/test_serializers.py::test_twos_compliment_64_serializer_loads'
	)

	epytest
}
