# Copyright 2021-2024 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

PYTHON_COMPAT=( python3_{9,10,11,12} )
DISTUTILS_USE_PEP517=setuptools
inherit distutils-r1 pypi

DESCRIPTION="Language detection library ported from Google's language-detection"
HOMEPAGE="https://github.com/Mimino666/langdetect"

LICENSE="Apache-2.0"
SLOT="0"
KEYWORDS="amd64 ~x86"

RDEPEND="dev-python/six[${PYTHON_USEDEP}]"

distutils_enable_tests pytest

src_prepare() {
	sed -i "s/'langdetect.tests'/'langdetect.tests', 'langdetect.profiles'/" setup.py || die

	distutils-r1_src_prepare
}
