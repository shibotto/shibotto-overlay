# Copyright 2021-2023 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

PYTHON_COMPAT=( python3_{9,10,11,12} )
DISTUTILS_USE_PEP517=setuptools
inherit distutils-r1 pypi

DESCRIPTION="Library for fast text representation and classification"
HOMEPAGE="https://github.com/facebookresearch/fastText"

LICENSE="MIT"
SLOT="0"
KEYWORDS="amd64 ~x86"
# I have no idea
RESTRICT="test"

DEPEND="
	dev-python/pybind11[${PYTHON_USEDEP}]
"
RDEPEND="
	${DEPEND}
	dev-python/numpy[${PYTHON_USEDEP}]
"

PATCHES=( "${FILESDIR}/${PN}-0.9.2-gcc13.patch" )

src_prepare() {
	sed -i "s/description-file/description_file/" setup.cfg || die

	distutils-r1_src_prepare
}
