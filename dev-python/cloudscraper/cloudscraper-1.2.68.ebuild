# Copyright 2019-2024 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

PYTHON_COMPAT=( python3_{9,10,11,12} )
PYTHON_REQ_USE="ssl"
DISTUTILS_USE_PEP517=setuptools
inherit distutils-r1

DESCRIPTION="Python module to bypass Cloudflare's anti-bot page"
HOMEPAGE="https://github.com/VeNoMouS/cloudscraper"
SRC_URI="https://github.com/VeNoMouS/${PN}/archive/${PV}.tar.gz -> ${P}.gh.tar.gz"

LICENSE="MIT"
SLOT="0"
KEYWORDS="amd64 ~x86"

RDEPEND="
	dev-python/pyparsing[${PYTHON_USEDEP}]
	dev-python/requests[${PYTHON_USEDEP}]
	dev-python/requests-toolbelt[${PYTHON_USEDEP}]
"
BDEPEND="
	test? (
		dev-python/responses[${PYTHON_USEDEP}]
		dev-python/js2py[${PYTHON_USEDEP}]
		net-libs/nodejs
	)
"

distutils_enable_tests pytest

python_test() {
	# Poorly maintained tests
	EPYTEST_DESELECT=(
		'tests/test_cloudscraper.py::TestCloudScraper::test_bad_interpreter_js_challenge1_16_05_2020'
		'tests/test_cloudscraper.py::TestCloudScraper::test_bad_solve_js_challenge1_16_05_2020'
		'tests/test_cloudscraper.py::TestCloudScraper::test_Captcha_challenge_12_12_2019'
		'tests/test_cloudscraper.py::TestCloudScraper::test_reCaptcha_providers'
	)

	epytest
}
