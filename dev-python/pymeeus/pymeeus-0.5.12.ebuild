# Copyright 2020-2024 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

DISTUTILS_USE_PEP517=setuptools
PYTHON_COMPAT=( python3_{9,10,11,12} )
PYPI_PN="PyMeeus"
PYPI_NO_NORMALIZE=1
inherit distutils-r1 pypi

DESCRIPTION="Library of astronomical algorithms in Python"
HOMEPAGE="https://github.com/architest/pymeeus"

LICENSE="LGPL-3"
SLOT="0"
KEYWORDS="amd64 ~x86"

distutils_enable_sphinx docs/source \
	'dev-python/sphinx-rtd-theme'
distutils_enable_tests pytest
