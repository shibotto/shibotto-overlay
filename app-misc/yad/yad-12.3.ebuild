# Copyright 2021-2023 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

inherit gnome2-utils xdg

DESCRIPTION="Yet Another Dialog"
HOMEPAGE="https://github.com/v1cont/yad"
SRC_URI="https://github.com/v1cont/${PN}/releases/download/v${PV}/${P}.tar.xz"

LICENSE="GPL-3"
SLOT="0"
KEYWORDS="amd64 ~x86"

IUSE="sourceview spell tools webkit"

DEPEND="
	x11-libs/gtk+:3
	sourceview? ( x11-libs/gtksourceview:3.0= )
	spell? ( app-text/gspell:= )
	webkit? ( net-libs/webkit-gtk:4 )
"
RDEPEND="${DEPEND}"
BDEPEND="
	dev-util/intltool
	virtual/pkgconfig
"

src_configure() {
	econf \
		$(use_enable sourceview) \
		$(use_enable spell) \
		$(use_enable tools icon-browser) \
		$(use_enable tools tools) \
		$(use_enable webkit html) \
		--disable-standalone \
		--enable-tray
}

pkg_postrm() {
	xdg_pkg_postrm
	gnome2_schemas_update
}

pkg_postinst() {
	xdg_pkg_postinst
	gnome2_schemas_update
}
