# Copyright 2020-2024 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

PYTHON_COMPAT=( python3_{9,10,11,12} )
inherit gnome2-utils meson optfeature python-single-r1 xdg

DESCRIPTION="A simple and modern GTK eBook reader"
HOMEPAGE="https://johnfactotum.github.io/foliate/"
SRC_URI="https://github.com/johnfactotum/${PN}/archive/${PV}.tar.gz -> ${P}.tar.gz"

LICENSE="
BSD-2
GPL-3
GPL-3+
MIT
"
SLOT="0"
KEYWORDS="amd64 ~x86"
REQUIRED_USE="${PYTHON_REQUIRED_USE}"

DEPEND=">=dev-libs/gjs-1.52"
RDEPEND="
	${DEPEND}
	${PYTHON_DEPS}
	|| (
		net-libs/webkit-gtk:4.1[introspection]
		net-libs/webkit-gtk:4[introspection]
	)
	sys-apps/bubblewrap
	x11-libs/gdk-pixbuf[jpeg]
	x11-libs/gtk+:3[introspection]
"
BDEPEND="
	${PYTHON_DEPS}
	dev-libs/appstream-glib
	sys-devel/gettext
	virtual/pkgconfig
"

PATCHES=(
	"${FILESDIR}/${P}-use-webkit-4.1-or-4.0.patch"
)

src_prepare() {
	# Force current Python implementation
	sed -i "s/\(const python =\).*/\1 GLib.find_program_in_path('$EPYTHON')/" src/epubView.js || die

	# Fix tests with network-sandbox
	sed -i "s/'validate'/'--nonet', 'validate'/" data/meson.build || die

	default
}

src_install() {
	meson_src_install

	python_optimize "${D}/usr/share/com.github.johnfactotum.Foliate/assets/KindleUnpack"

	dosym com.github.johnfactotum.Foliate /usr/bin/foliate
}

pkg_postinst() {
	gnome2_schemas_update
	xdg_pkg_postinst

	optfeature "text-to-speech" app-accessibility/espeak
	optfeature "offline dictionaries" app-text/dictd
	optfeature "spellchecking in annotations" app-text/gspell[introspection]
	optfeature "displaying language and region names" app-text/iso-codes
	optfeature "adaptive window layout" gui-libs/libhandy[introspection]
	elog
	elog "To enable auto-hyphenation install the hyphenation rules for your"
	elog "preferred languages (app-dicts/myspell-*)."
}

pkg_postrm() {
	gnome2_schemas_update
	xdg_pkg_postrm
}
