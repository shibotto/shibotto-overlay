# Shibotto overlay

Software I use on top of Gentoo.

## Usage

You shouldn't. Things in here may be obsolete or heavily patched. If you must, please refer to [Gentoo Wiki](https://wiki.gentoo.org/wiki//etc/portage/repos.conf).

## Keywords rationale

### Stable

Software I regularly use. Everything should compile, (preferably) pass tests and work fine.

### Testing

Known issue, missing features or things I've yet to fully test in general. x86 defaults to testing since I don't plan to spin up a x86_32 VM.

### Missing keywords

Broken or unfinished ebuilds, synced just for my own convenience. Also live ebuilds (\*-9999) since they are non-deterministic.
