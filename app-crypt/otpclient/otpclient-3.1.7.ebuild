# Copyright 2020-2023 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

inherit xdg cmake

MY_PN="OTPClient"

DESCRIPTION="OTP client written in C/GTK that supports both TOTP and HOTP"
HOMEPAGE="https://github.com/paolostivanin/OTPClient"
SRC_URI="https://github.com/paolostivanin/${MY_PN}/archive/v${PV}.tar.gz -> ${P}.tar.gz"

LICENSE="GPL-3"
SLOT="0"
KEYWORDS="amd64 ~x86"

RDEPEND="
	>=app-crypt/libsecret-0.20
	>=dev-libs/glib-2.64:2
	>=dev-libs/jansson-2.12.0:=
	>=dev-libs/libcotp-2.0.0:=
	>=dev-libs/libgcrypt-1.6:0=
	>=dev-libs/libzip-1:0=
	>=dev-libs/protobuf-3.6.0:=
	>=dev-libs/protobuf-c-1.3.0:=
	>=media-gfx/qrencode-4.0.2
	>=media-gfx/zbar-0.20
	>=media-libs/libpng-1.6.30:0=
	>=sys-apps/util-linux-2.34
	>=x11-libs/gtk+-3.24:3
"
DEPEND="${RDEPEND}"
BDEPEND="virtual/pkgconfig"

S="${WORKDIR}/${MY_PN}-${PV}"

src_install() {
	cmake_src_install
	gunzip "${ED}"/usr/share/man/man1/*.1.gz || die
}
